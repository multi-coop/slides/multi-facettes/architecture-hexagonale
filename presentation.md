---
title: multi-facettes - Architecture hexagonale
subtitle: Arrondir les angles de son code
author: multi.coop
date: 21 mai 2024
title-slide-attributes:
    data-background-image: "static/logo.svg, static/logo_client.svg"
    data-background-size: "auto 10%, auto 12%"
    data-background-position: "95% 95%, 70% 97%"
---


# C'est quoi ?

  ![ ](images/batiment-hexagone.jpg)
  

## Un peu de théorie
 
- Modèle d'organisation du code assurant la répartition des responsabilités. 
- (Plutot) Back-end
- Conceptualisé par [Alistair Cockburn](https://en.wikipedia.org/wiki/Alistair_Cockburn)

## A la base, un patron de conception 

![Services fortement couplés](images/couple.png)

Ex : Appel à une BDD ou à une API tierce

## A la base, un patron de conception 

![Services découplés](images/decouple.png)

## Contexte d'architecture

 - **Domain** : le coeur de l'application contenant toute l'intelligence métier
 - **Controller** : les points d'entrée permettant à des services tiers ou des utilisateurs de faire appel au *Domain* 
 - **Infra** : les services auxquels fait appel le *Domain*

## Comment les faire interagir ?

## Domaine
<img src="images/domain.png" height="550px"/>


## Domaine 

> - Il n'a **pas** connaissance des **controllers**.
> - Il ne connait que les **interfaces** de l'**infra**.

## Controllers 
<img src="images/controllers.png" height="550px"/>


## Controllers 

> - Les controllers ne peuvent utiliser que la partie publique du **domaine**.
> - Ils n'ont **pas** connaissance de l'**infra**  

## Infra

<img src="images/infra.png" height="550px"/>

## Infra

> - L'infra ne peut utiliser que le **modèle** public du **domaine**  
> - Elle n'a **pas** connaissance des **controllers**

## Injection de dépendances

<img src="images/dependencies.png" height="550px"/>

## Injection de dépendances

<img src="images/injection.png"/>

# À quoi ça sert ? 

![ ](images/emporte-piece-hexagone.jpeg)

## Découplage, découplage, découplage

> L'architecture, c'est l'art du découplage
>

 [Jean-Paul Figer]{.text-micro}
 
 [Jean-Paul Figer est entré en informatique avant même que le mot soit inventé.]{.text-nano}

## Pourquoi découpler

> Le couplage est une métrique indiquant le niveau d'interaction entre deux ou plusieurs composants logiciels.

-----

### Testabilité

Plus le couplage est important, plus il est difficile de tester un composant sans l'autre.

![ ](images/tests.png)

-----

### Lisibilité, maintenabilité et évolutivité

L'architecture hexagonale sépare les préoccupations : le code métier est très clairement exposé dans le domaine. 

Cette séparation des préoccupations permet également de diminuer la charge mentale pour travailler sur le code.

-----

### Standardisation d'un modèle mental

- Architecture peu contraignante qui s'adapte à tout type de projet
- À nouveau gain de charge mentale en appliquant de manière systématique

-----

### Les points négatifs

- Est plus difficile à mettre en oeuvre lorsque les dépendances sont elles-mêmes très couplées (frameworks - django, frictionless etc.)
- Code plus verbeux et moins facile à naviguer pour quelqu'un qui n'est pas familier avec l'architecture

# L'archi hexa dans la vraie vie

![ ](images/animals-hexagons.png)

---

* GDR - (SQLAlchemy) - Erica
* TEE - (arborescence) - Pierre
* tentative sur Validata
* Agatha - Language agnostique 

# Proposition pour poursuivre en Séminaire

* une matinée en mode code-along ou kata pour mettre les mains dans le cambouis 